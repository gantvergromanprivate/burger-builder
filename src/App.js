import React, { useEffect, Suspense } from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './hoc/Layout';
import BurgerBuilder from './containers/BurgerBuilder';
import Logout from './containers/Auth/Logout';

import * as actions from './store/actions';

const Checkout = React.lazy(() => {
  return import('./containers/Checkout');
});
const Orders = React.lazy(() => {
  return import('./containers/Orders');
});
const Auth = React.lazy(() => {
  return import('./containers/Auth');
});

const app = props => {
  useEffect( () => {
    props.onTryAutoSignup();
  }, []);

  let routes = (
    <Switch>
      <Route path="/auth" render={props => <Auth {...props}/>} />
      <Route path="/" exact component={BurgerBuilder} />
      <Redirect to="/" />
    </Switch>
  );
  if (props.isAuth) {
    routes = (
      <Switch>
        <Route path="/checkout" render={props => <Checkout {...props}/>} />
        <Route path="/orders" render={props => <Orders {...props}/>} />
        <Route path="/logout" component={Logout} />
        <Route path="/auth" render={props => <Auth {...props}/>} />
        <Route path="/" component={BurgerBuilder} />
        <Redirect to="/" />
      </Switch>
    )
  }
  return (
    <div>
      <Layout>
        <Suspense fallback={<div>Loading...</div>}>
          {routes}
        </Suspense>
      </Layout>
    </div>
  );
}

const mapStateToProps = ({ auth }) => ({
  isAuth: auth.token !== null,
})

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState()),
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(app));
