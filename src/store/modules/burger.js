import { updateObject } from '../../shared/utility';

const INGREDIENT_PRICE = {
  salad: 0.5,
  bacon: 0.7,
  cheese: 0.4,
  meat: 1.3,
}
export const BASE_PRICE = 4;

export const addIngredient = (ingredients, type, totalPrice) => {
  const newIngredients = updateObject(ingredients, { [type]: ingredients[type] + 1 });
  const newPrice = totalPrice + INGREDIENT_PRICE[type];
  return {
    ingredients: newIngredients,
    totalPrice: newPrice,
    building: true,
  }
}

export const removeIngredient = (ingredients, type, totalPrice) => {
  const newIngredients = updateObject(ingredients, { [type]: ingredients[type] - 1 });
  const newPrice = totalPrice - INGREDIENT_PRICE[type];
  return {
    ingredients: newIngredients,
    totalPrice: newPrice,
    building: true,
  };
}

export const setIngredients = ({
  salad,
  bacon,
  cheese,
  meat,
}) => {
  return {
    ingredients: { // set the right order of ingredients
      salad,
      bacon,
      cheese,
      meat,
    },
    totalPrice: BASE_PRICE,
    building: false,
    error: false,
  };

}
