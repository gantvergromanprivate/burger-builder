const BASE_PATH = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/';
const API_KEY = 'AIzaSyCa-GB76RWKZm9NBsCng2C6h79OwjQL4Gc';

export const SIGN_UP = `${BASE_PATH}signupNewUser?key=${API_KEY}`;
export const SIGN_IN = `${BASE_PATH}verifyPassword?key=${API_KEY}`;

export const TOKEN = 'token';
export const EXPIRATION_DATE = 'expirationDate';
export const USER_ID = 'userId';
