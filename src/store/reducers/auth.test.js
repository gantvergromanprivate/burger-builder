import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

const initState = {
  token: null,
  userId: null,
  error: null,
  loading: null,
  authRedirectPath: '/',
};
const VALID_TOKEN = 'some-token';
const VALID_USER_ID = 'some-user-id';

describe('auth reducer', () => {
  it('should return the init state', () => {
    expect(reducer(undefined, {})).toEqual(initState);
  });
  it('should store the token upon login', () => {
    expect(reducer(
      initState,
      {
        type: actionTypes.AUTH_SUCCESS,
        idToken: VALID_TOKEN,
        userId: VALID_USER_ID,
      })
    ).toMatchObject({
      token: VALID_TOKEN,
      userId: VALID_USER_ID,
    });
  })
});
