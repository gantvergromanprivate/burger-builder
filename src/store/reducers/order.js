import * as actionType from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initState = {
  orders: [],
  loading: false,
  purchased: false,
}

const purchaseInit = (state, _) => {
  return updateObject( state, { purchased: false });
};

const purchaseBurgerStart = (state, _) => {
  return updateObject(state, { loading: true });
};

const purchaseBurgerSuccess = (state, { order, id }) => {
  const newOrder = updateObject(order, { id });
  return updateObject(state, {
    loading: false,
    purchased: true,
    orders: state.orders.concat(newOrder),
  });
};

const purchaseBurgerFail = (state, _) => {
  return updateObject(state, { loading: false });
};

const fetchOrdersStart = (state, _) => {
  return updateObject(state, { loading: true });
};

const fetchOrdersSuccess = (state, { orders }) => {
  return updateObject(state, { orders: orders, loading: false });
};

const fetchOrdersFail = (state, _) => {
  return updateObject(state, { loading: false });
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case actionType.PURCHASE_INIT: return purchaseInit(state, action);
    case actionType.PURCHASE_BURGER_START: return purchaseBurgerStart(state, action);
    case actionType.PURCHASE_BURGER_SUCCESS: return purchaseBurgerSuccess(state, action);
    case actionType.PURCHASE_BURGER_FAIL: return purchaseBurgerFail(state, action);
    case actionType.FETCH_ORDERS_START: return fetchOrdersStart(state, action);
    case actionType.FETCH_ORDERS_SUCCESS: return fetchOrdersSuccess(state, action);
    case actionType.FETCH_ORDERS_FAIL: return fetchOrdersFail(state, action);
    default: return state;
  }
}

export default reducer;
