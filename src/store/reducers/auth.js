import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initState = {
  token: null,
  userId: null,
  error: null,
  loading: null,
  authRedirectPath: '/',
}

const authStart = (state, _) => {
  return updateObject(state, { error: null, loading: true });
};

const authSuccess = (state, { idToken, userId }) => {
  return updateObject( 
    state,
    {
      token: idToken,
      userId,
      error: null,
      loading: false,
      // authRedirectPath: '/',
    }
  );
};

const authFail = (state, { error }) => {
  return updateObject(state, { error, loading: false });
};

const authLogout = (state, _) => {
  return updateObject(state, { token: null, userId: null });
}

const setAuthRedirectPath = (state, { path }) => {
  return updateObject(state, { authRedirectPath: path })
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.AUTH_START: return authStart(state, action);
    case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
    case actionTypes.AUTH_FAIL: return authFail(state, action);
    case actionTypes.AUTH_LOGOUT: return authLogout(state, action);
    case actionTypes.SET_AUTH_REDIRECT_PATH: return setAuthRedirectPath(state, action);
    default: return state;
  }
}

export default reducer;
