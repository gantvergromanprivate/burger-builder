import * as actionTypes from '../actions/actionTypes';
import {
  BASE_PRICE,
  addIngredient,
  removeIngredient,
  setIngredients,
} from '../modules/burger';
import { updateObject } from '../../shared/utility';

const initState = {
  ingredients: null,
  totalPrice: BASE_PRICE,
  error: false,
  building: false,
}

const addIngredientReducer = (state, { ingredient }) => {
  const { ingredients, totalPrice } = state;
  const newState = addIngredient(ingredients, ingredient, totalPrice);
  return updateObject(state, newState);
};

const removeIngredientReducer = (state, { ingredient }) => {
  const { ingredients, totalPrice } = state;
  const newState = removeIngredient(ingredients, ingredient, totalPrice);
  return updateObject(state, newState);
};

const setIngredientsReducer = (state, { ingredients }) => {
  const newState = setIngredients(ingredients);
  return updateObject(state, newState);
}

const fetchIngredientsFailedReducer = (state, _) => {
  const newState = { error: true };
  return updateObject(state, newState);
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case actionTypes.ADD_INGREDIENT: return addIngredientReducer(state, action);
    case actionTypes.REMOVE_INGREDIENT: return removeIngredientReducer(state, action);
    case actionTypes.SET_INGREDIENTS: return setIngredientsReducer(state, action);
    case actionTypes.FETCH_INGREDIENTS_FAILED: return fetchIngredientsFailedReducer(state, action);
    default: return state;
  }
}

export default reducer;
