import { put } from 'redux-saga/effects';
import axios from '../../axios-orders';

import * as actions from '../actions';

export function* purchaseBurgerSaga(action) {
  const { order, token } = action;
  yield put(actions.purchaseBurgerStart());
  try {
    const response = yield axios.post(`/orders.json?auth=${token}`, order)
    yield put(actions.purchaseBurgerSuccess(response.data.name, order));
  } catch(error) {
    yield put(actions.purchaseBurgerFail(error));
  }
};

export function* fetchOrdersSaga(action) {
  const { token, userId } = action;
  yield put(actions.fetchOrdersStart());

  try {
    const queryParams = `?auth=${token}&orderBy="userId"&equalTo="${userId}"`;
    const response = yield axios.get(`/orders.json${queryParams}`);
    const orders = Object.entries(response.data).map(([id, value]) => ({
      ...value,
      id
    }));
    yield put(actions.fetchOrdersSuccess(orders));
  } catch(error) {
    yield put(actions.fetchOrdersFail(error));
  }
};
