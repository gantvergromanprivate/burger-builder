import { put, call, delay } from 'redux-saga/effects';
// import { delay } from 'redux-saga';
import axios from 'axios';

import * as actions from '../actions';
import {
  TOKEN,
  EXPIRATION_DATE,
  USER_ID,
  SIGN_IN,
  SIGN_UP,
} from '../modules/auth';

export function* logoutSaga(action) {
  yield call([localStorage, "removeItem"], TOKEN);
  yield call([localStorage, "removeItem"], EXPIRATION_DATE);
  yield call([localStorage, "removeItem"], USER_ID);
  yield put(actions.logoutSucced());
}

export function* checkAuthTimroutSaga(action) {
  yield delay(action.expirationTime * 1000);
  yield put(actions.logout());
}

export function* authSaga(action) {
  const { email, password, isSignUp } = action;
  yield put(actions.authStart());
  const authData = {
    email,
    password,
    returnSecureToken: true,
  }

  try {
    const response = yield axios.post(isSignUp ? SIGN_UP : SIGN_IN, authData);
    const { idToken, localId, expiresIn } = response.data;
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
  
    yield localStorage.setItem(TOKEN, idToken);
    yield localStorage.setItem(EXPIRATION_DATE, expirationDate);
    yield localStorage.setItem(USER_ID, localId);
  
    yield put(actions.authSuccess(idToken, localId));
    yield put(actions.checkAuthTimeOut(expiresIn));
  } catch (error) {
    yield put(actions.authFail(error.response.data.error));
  }
}

export function* authCheckStateSaga(action) {
  const token = yield localStorage.getItem(TOKEN);
  if (!token) {
    yield put((actions.logout()));
  } else {
    const expirationDate = yield new Date(localStorage.getItem(EXPIRATION_DATE));
    if (expirationDate > new Date()) {
      const userId = yield localStorage.getItem(USER_ID);
      yield put(actions.authSuccess(token, userId));
      yield put(actions.checkAuthTimeOut((expirationDate.getTime() - new Date().getTime()) / 1000));
    } else {
      yield put(actions.logout());
    }
  }
}

