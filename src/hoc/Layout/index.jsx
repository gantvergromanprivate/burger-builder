import React, { useState } from 'react';
import { connect } from 'react-redux';

import Aux from '../Aux';
import Toolbar from '../../components/Navigation/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer';
import classes from './Layout.module.css';

const layout = props => {
  const [sideDrawerIsVisible, setSideDrawerIsVisible] = useState(false);
  
  const sideDrawerClosedHandler = () => {
    setSideDrawerIsVisible(false);
  }
  const sideDrawerToggleHandler = () => {
    setSideDrawerIsVisible(!sideDrawerIsVisible);
  }

  return (
    <Aux>
      <Toolbar
        isAuth={props.isAuth}
        drawerToggleClicked={sideDrawerToggleHandler} />
      <SideDrawer
        isAuth={props.isAuth}
        open={sideDrawerIsVisible}
        closed={sideDrawerClosedHandler} />
      <main className={classes.Content}>
        {props.children}
      </main>
    </Aux>
  );
}

const mapStateToProps = ({ auth }) => ({
  isAuth: auth.token !== null,
})

export default connect(mapStateToProps)(layout);
