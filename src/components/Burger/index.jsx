import React from 'react';
import BurgerIngredient from './BurgerIngredient';

import classes from './Burger.module.css';

const burger = (props) => {
  const { ingredients } = props;
  let burgerIngredients = Object.entries(ingredients)
    .map(([key, value]) => {
      return [...Array(value)].map((_, i) => (
        <BurgerIngredient key={`${key}_${i}`} type={key} />
      ))
    })
    .reduce((arr, el) => [...arr, ...el], []);
  if (burgerIngredients.length === 0) {
    burgerIngredients = <p>Please start adding ingredients!</p>
  }
  return (
    <div className={classes.Burger}>
      <BurgerIngredient type='bread-top' />
      {burgerIngredients}
      <BurgerIngredient type='bread-bottom' />
    </div>
  )
}

export default burger;