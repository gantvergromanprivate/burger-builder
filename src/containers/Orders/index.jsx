import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import Order from '../../components/Order';
import axios from '../../axios-orders';
import withErrorHandler from '../../hoc/withErrorHandler';
import * as actions from '../../store/actions';
import Spinner from '../../components/UI/Spinner';

const orders = props => {
  useEffect(() => {
    props.onFetchOrders(props.token, props.userId);
  }, []);

  let orders = <Spinner />;
  if (!props.loading) {
    orders = props.orders.map(({ id, ingredients, price }) => (
      <Order
        key={id}
        ingredients={ingredients}
        price={price}/>
    ));
  }
  return orders;
}

const mapStateToProps = ({ order, auth }) => ({
  orders: order.orders,
  loading: order.loading,
  token: auth.token,
  userId: auth.userId,
})

const mapDispatchToProps = dispatch => ({
  onFetchOrders: (token, userId) => dispatch(actions.fetchOrders(token, userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(orders, axios));
