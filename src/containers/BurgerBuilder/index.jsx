import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger';
import BuildControls from '../../components/Burger/BuildControls';
import Modal from '../../components/UI/Modal';
import OrderSummary from '../../components/Burger/OrderSummary';
import Spinner from '../../components/UI/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler';

import * as actions from '../../store/actions';
import axios from '../../axios-orders';

export const burgerBuilder = props => {
  const [purchasing, setPurchasing] = useState(false);

  useEffect(() => {
    props.onInitIngredients();
  }, [])

  const updatePurchasableState = ingredients => {
    const result = Object
      .values(ingredients)
      .some(value => value > 0);
    return result;
  }

  const purchaseHandler = () => {
    if (props.isAuth) {
      setPurchasing(true);
    } else {
      props.onSetAuthRedirectPath('/checkout');
      props.history.push('/auth');
    }
  }

  const purchaseCancelHandler = () => {
    setPurchasing(false);
  }

  const purchaseContinueHandler = () => {
    props.onInitPurchase();
    props.history.push('/checkout');
  }

  let burger = props.error ? <p>Ingredients can't be loaded</p> : <Spinner />;
  let orderSummary = null;
  if (props.ingredients) {
    const disabledInfo = Object.entries(props.ingredients)
      .reduce((info, [key, value]) => {
        info[key] = value <= 0;
        return info;
      }, {});

    burger = (
      <Aux>
        <Burger ingredients={props.ingredients}/>
        <BuildControls 
          isAuth={props.isAuth}
          ingredientAdded={props.onIngredientAdded}
          ingredientRemoved={props.onIngredientRemoved}
          disabled={disabledInfo}
          price={props.totalPrice}
          purchasable={updatePurchasableState(props.ingredients)}
          ordered={purchaseHandler} />
      </Aux>
    );
    orderSummary = <OrderSummary
      ingredients={props.ingredients}
      price={props.totalPrice}
      purchaseCancelled={purchaseCancelHandler}
      purchaseContinued={purchaseContinueHandler} />
  }

  return (
    <Aux>
      <Modal show={purchasing} modalClosed={purchaseCancelHandler}>
        {orderSummary}
      </Modal>
      {burger}
    </Aux>
  )
}

const mapStateToProps = ({ burgerBuilder, auth }) => ({
  ingredients: burgerBuilder.ingredients,
  totalPrice: burgerBuilder.totalPrice,
  error: burgerBuilder.error,
  isAuth: auth.token !== null,
});

const mapDispatchToProps = dispatch => ({
  onIngredientAdded: (ingredient) => dispatch(actions.addIngrediend(ingredient)),
  onIngredientRemoved: (ingredient) => dispatch(actions.removeIngrediend(ingredient)),
  onInitIngredients: () => dispatch(actions.initIngredients()),
  onInitPurchase: () => dispatch(actions.purchaseInit()),
  onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path)),
});

const withErrorBurgerBuilder = withErrorHandler(burgerBuilder, axios);
const connectedBurgerBuilder = connect(mapStateToProps, mapDispatchToProps)(withErrorBurgerBuilder);
export default connectedBurgerBuilder;
