import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Input from '../../components/UI/Input';
import Button from '../../components/UI/Button';
import Spinner from '../../components/UI/Spinner';

import classes from './Auth.module.css';

import * as actions from '../../store/actions';
import { updateObject, checkValidity } from '../../shared/utility';

const auth = props => {
  const [isSignup, setIsSignup] = useState(true);
  const [authForm, setAuthForm] = useState({
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Mail Address',
      },
      value: '',
      validation: {
        required: true,
        isEmail: true,
      },
      valid: false,
      touched: false,
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Password',
      },
      value: '',
      validation: {
        required: true,
        minLength: 6,
      },
      valid: false,
      touched: false,
    },
  });

  useEffect(() => {
    if (!props.building && props.authRedirectPath !== '/') {
      props.onSetAuthRedirectPath();
    }
   }, []);

  const inputChangedHandler = (event, key) => {
    const controls = updateObject(
      authForm,
      {
        [key]: updateObject(
          authForm[key],
          {
            value: event.target.value,
            valid: checkValidity(event.target.value, authForm[key].validation),
            touched: true,
          }
        )
      }
    );
    setAuthForm(controls);
  }

  const switchAuthModeHandler = () => setIsSignup(!isSignup);

  const submitHandler = (event) => {
    event.preventDefault();
    props.onAuth(
      authForm.email.value, 
      authForm.password.value, 
      isSignup,
    );
  }

  let inputElements = Object.entries(authForm).map(([key, formElement]) => (
    <Input 
      key={key}
      elementType={formElement.elementType}
      elementConfig={formElement.elementConfig}
      value={formElement.value}
      invalid={!formElement.valid}
      shouldValidate={formElement.validation}
      touched={formElement.touched}
      changed={(event) => inputChangedHandler(event, key)}/>
  ));

  if (props.loading) {
    inputElements = <Spinner />
  }
  let errorMessage = null;
  if (props.error) {
    errorMessage = <p>{props.error.message}</p>
  }

  let authRedirect = null;
  if (props.isAuth) {
    authRedirect = <Redirect to={props.authRedirectPath} />
  }

  return (
    <div className={classes.Auth}>
      {authRedirect}
      <h2>{isSignup ? 'SIGNUP' : 'SIGNIN'}</h2>
      {errorMessage}
      <form onSubmit={submitHandler}>
        {inputElements}
        <Button btnType="Success">SUBMIT</Button>
      </form>
      <Button 
        btnType="Danger"
        clicked={switchAuthModeHandler}>
        SWITCH TO {isSignup ? 'SIGNIN' : 'SIGNUP'}
      </Button>
    </div>
  );
}

const mapStateToProps = ({ auth, burgerBuilder }) => ({
  loading: auth.loading,
  error: auth.error,
  isAuth: auth.token !== null,
  building: burgerBuilder.building,
  authRedirectPath: auth.authRedirectPath,
})

const mapDispatchToProps = dispatch => ({
  onAuth: (email, password, isSignup) => dispatch(actions.auth(email, password, isSignup)),
  onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/')),
})

export default connect(mapStateToProps, mapDispatchToProps)(auth);
