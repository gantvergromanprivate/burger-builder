import React, { useState } from 'react';
import { connect } from 'react-redux';

import Button from '../../../components/UI/Button';
import Spinner from '../../../components/UI/Spinner';
import Input from '../../../components/UI/Input';

import axios from '../../../axios-orders';
import withErrorHandler from '../../../hoc/withErrorHandler';
import { updateObject, checkValidity } from '../../../shared/utility';

import * as orderActions from '../../../store/actions';

import classes from './ContactData.module.css';

const contactData = (props) => {
  const [orderForm, setOrderForm] = useState({
    name: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Your Name',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    street: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Street',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    zip: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'ZIP Code',
      },
      value: '',
      validation: {
        required: true,
        minLength: 5,
        maxLength: 5,
      },
      valid: false,
      touched: false,
    },
    country: {
      elementType: 'input',
      elementConfig: {
        type: 'text',
        placeholder: 'Country',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Your E-Mail',
      },
      value: '',
      validation: {
        required: true,
      },
      valid: false,
      touched: false,
    },
    deliveryMethod: {
      elementType: 'select',
      elementConfig: {
        options: [
          { value: 'fastest', displayValue: 'Fastest'},
          { value: 'cheapest', displayValue: 'Cheapest'},
        ],
      },
      value: 'fastest',
      validation : {},
      valid: true,
      touched: false,
    },
  });
  const [formIsValid, setFormIsValid] = useState(false);

  const orderHandler = (event) => {
    event.preventDefault();
    const orderData = Object.entries(orderForm).reduce((res, [key, { value }]) => {
      res[key] = value;
      return res;
    }, {})
    const order = {
      ingredients: props.ingredients,
      price: props.totalPrice,
      orderData,
      userId: props.userId,
    };
    props.onOrderBurger(order, props.token);
  }

  const inputChangedHandler = (event, key) => {
    const { value } = event.target;
    const updatedFormElement = updateObject(
      orderForm[key],
      {
        value,
        valid: checkValidity(value, orderForm[key].validation),
        touched: true,
      }
    );
    const updatedOrderForm = updateObject(
      orderForm, 
      { [key]: updatedFormElement }
    );
    
    const formIsValid = Object
      .values(updatedOrderForm)
      .reduce((res, { valid }) => res && valid, true);
    
    setOrderForm(updatedOrderForm);
    setFormIsValid(formIsValid);
  }

  const inputElements = Object.entries(orderForm).map(([key, formElement]) => (
    <Input 
      key={key}
      elementType={formElement.elementType}
      elementConfig={formElement.elementConfig}
      value={formElement.value}
      invalid={!formElement.valid}
      shouldValidate={formElement.validation}
      touched={formElement.touched}
      changed={(event) => inputChangedHandler(event, key)}/>
  ));
  let form = (
    <form onSubmit={orderHandler}>
      {inputElements}
      <Button btnType="Success" disabled={!formIsValid}>ORDER</Button>
    </form>
  );
  if (props.loading) {
    form = <Spinner />
  }
  return (
    <div className={classes.ContactData}>
      <h4> Enter your Contact Data</h4>
      {form}
    </div>
  )
}

const mapStateToProps = ({ burgerBuilder, order, auth }) => ({
  ingredients: burgerBuilder.ingredients,
  totalPrice: burgerBuilder.totalPrice,
  loading: order.loading,
  token: auth.token,
  userId: auth.userId,
});

const mapDispatchToProps = dispatch => ({
  onOrderBurger: (order, token) => dispatch(orderActions.purchaseBurger(order, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(contactData, axios));